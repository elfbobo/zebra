package com.guosen.zebra.gateway.route.model;

/**
 * 路由信息
 */
public class RouteInfo {

    /**
     * 微服务全称
     */
    private String serviceName;

    /**
     * 版本
     */
    private String version;

    /**
     * 组
     */
    private String group;

    /**
     * 方法
     */
    private String method;

    /**
     * 配置的 URL 前缀
     */
    private String urlPrefix;

    private RouteInfo(){}

    public static class Builder {
        private RouteInfo routeInfo = new RouteInfo();

        public Builder serviceName(String serviceName) {
            routeInfo.setServiceName(serviceName);
            return this;
        }

        public Builder version(String version) {
            routeInfo.setVersion(version);
            return this;
        }

        public Builder group(String group) {
            routeInfo.setGroup(group);
            return this;
        }

        public Builder method(String method) {
            routeInfo.setMethod(method);
            return this;
        }

        public Builder urlPrefix(String urlPrefix) {
            routeInfo.setUrlPrefix(urlPrefix);
            return this;
        }

        public RouteInfo build() {
            return routeInfo;
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public String toString() {

        return "{" +
                "serviceName : " + serviceName + "," +
                "version : " + version + "," +
                "group : " + group + "," +
                "method : " + method + "," +
                "}";
    }


    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public void setUrlPrefix(String urlPrefix) {
        this.urlPrefix = urlPrefix;
    }
}
